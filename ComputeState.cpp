//
// Created by yessergire on 27.6.2015.
//

#include "ComputeState.h"

ComputeState::ComputeState(const IteratingFunction *complexFunction, const Color *color)
        : complexFunction(complexFunction), color(color) {
}

void ComputeState::setComplexFunction(const IteratingFunction &complexFunction) {
    this->radius = radius;
}

const IteratingFunction *ComputeState::getComplexFunction() const {
    return complexFunction;
}

void ComputeState::setColor(const Color &color) {
    this->color = &color;
}

const Color *ComputeState::getColor() const {
    return color;
}

void ComputeState::setRadius(double radius) {
    this->radius = radius;
}

double ComputeState::getRadius() const {
    return radius;
}

void ComputeState::setCenter(const Complex &center) {
    this->center = center;
}

Complex ComputeState::getCenter() const {
    return center;
}

Complex ComputeState::getPoint(int index, int width, int height) const {
    double asp = 1.0 * width / height;
    return complexFunction->calculate(Complex(
            (center.real() - radius * asp) + (2.0f * radius * asp / width) * (index % width),
            (center.imag() - radius) + (2.0f * radius / height) * (index / width)
    ));

}

unsigned char ComputeState::getPointIterations(int index, int width, int height) const {
    double asp = 1.0 * width / height;
    return complexFunction->calculateIterations(Complex(
            (center.real() - radius * asp) + (2.0f * radius * asp / width) * (index % width),
            (center.imag() - radius) + (2.0f * radius / height) * (index / width)
    ));
}

int ComputeState::getRGB(int x, int y, int width, int height) const {
    return color->getColor(getPoint(x*width + y, width, height));
}

unsigned char ComputeState::getIterations(int x, int y, int width, int height) const {
    return getPointIterations(x*width + y, width, height);
}
