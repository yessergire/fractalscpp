#include <iostream>
#include "WindowDrawer.h"

#define SIZE 1500

using namespace std;

int main() {
    cout << "Creating a png picture." << endl;
    WindowDrawer drawer;
    drawer.draw("testing....png", SIZE, SIZE);
    cout << "Png saved." << endl;
    return 0;
}
