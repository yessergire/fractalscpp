//
// Created by yessergire on 27.6.2015.
//

#include "WindowDrawer.h"
#include "ComputeState.h"
#include "PNGWriter/PngWriter.h"
#include "Color/PhasePlotWithContourLines.h"
#include "Functions/JuliaSet.h"

void WindowDrawer::draw(const char* filename, unsigned width, unsigned height) {
    PngWriter *writer = new PngWriter(filename, width, height);
    JuliaSet s;
    PhasePlotWithContourLines c;
    ComputeState state(&s, &c);
    state.setRadius(1);
    int *RGBs = new int[width];

    for (int y=0 ; y<height ; y++) {
        for (int x=0 ; x<width ; x++) {
            RGBs[x] = state.getRGB(y,x,width, height);
        }
        writer->writeRGBRow(RGBs);
    }
    writer->close();
}
