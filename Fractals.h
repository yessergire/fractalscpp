//
// Created by yessergire on 27.6.2015.
//

#ifndef FRACTALS_FRACTALS_H
#define FRACTALS_FRACTALS_H

#include <complex>

typedef long double TYPE;
typedef std::complex<TYPE> Complex;

#endif //FRACTALS_FRACTALS_H
