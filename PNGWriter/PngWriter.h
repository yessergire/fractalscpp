//
// Created by yessergire on 30.6.2015.
//

#ifndef FRACTALS_PNGWRITER_H
#define FRACTALS_PNGWRITER_H

#include <png.h>

class PngWriter {
public:
    PngWriter(const char *filename, unsigned width, unsigned height);
    void writeRGBRow(const int *RGBs);
    void close();
    ~PngWriter();

private:
    unsigned m_width;
    unsigned m_height;
    FILE *file;
    png_structp png_ptr;
    png_infop info_ptr;
    png_bytep row;
};


#endif //FRACTALS_PNGWRITER_H
