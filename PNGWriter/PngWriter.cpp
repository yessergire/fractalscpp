//
// Created by yessergire on 30.6.2015.
//

#include <malloc.h>
#include "PngWriter.h"

PngWriter::PngWriter(const char *filename, unsigned width, unsigned height) : m_width(width), m_height(height) {
    file = fopen(filename, "wb");
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    // Initialize info structure
    info_ptr = png_create_info_struct(png_ptr);
    // Setup Exception handling
    setjmp(png_jmpbuf(png_ptr));
    png_init_io(png_ptr, file);
    // Write header (8 bit colour depth)
    png_set_IHDR(png_ptr, info_ptr, width, height,
                 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
    png_write_info(png_ptr, info_ptr);
    row = (png_bytep) malloc(width * sizeof(png_byte));
}

void PngWriter::writeRGBRow(const int *RGBs) {
    for (unsigned i = 0; i < m_width; i++) {
        row[i * 3] = (RGBs[i] >> 16) & 0xFF;
        row[i * 3 + 1] = (RGBs[i] >> 8) & 0xFF;
        row[i * 3 + 2] = (RGBs[i] >> 0) & 0xFF;
    }
    png_write_row(png_ptr, row);
}

void PngWriter::close() {
    png_write_end(png_ptr, NULL);
    fflush(file);
    fclose(file);
}

PngWriter::~PngWriter() {
    free(row);
    if (info_ptr != NULL) png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
    png_destroy_write_struct(&png_ptr, (png_infopp) NULL);
}
