//
// Created by yessergire on 27.6.2015.
//

#ifndef FRACTALS_WINDOWDRAWER_H
#define FRACTALS_WINDOWDRAWER_H


class WindowDrawer {
public:
    void draw(const char* filename, unsigned width, unsigned height);
};


#endif //FRACTALS_WINDOWDRAWER_H
