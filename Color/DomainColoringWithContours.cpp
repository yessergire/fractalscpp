//
// Created by yessergire on 30.6.2015.
//

#include "DomainColoringWithContours.h"

int DomainColoringWithContours::getColor(const Complex &c) const {
    double hue = arg(c) / (2 * M_PI) + 1;
    double brightness = log(norm(c)) / log(2);
    brightness = brightness - floor(brightness);
    return HSBtoRGB((float) hue, 1, (float) pow(brightness, 0.2));

}
