//
// Created by yessergire on 30.6.2015.
//

#ifndef FRACTALS_DOMAIN_COLORING_H
#define FRACTALS_DOMAIN_COLORING_H

#include "Color.h"

class DomainColoring : public Color {

public:
    virtual int getColor(const Complex &c) const override;
};


#endif //FRACTALS_DOMAIN_COLORING_H
