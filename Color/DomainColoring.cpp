//
// Created by yessergire on 30.6.2015.
//

#include "DomainColoring.h"

int DomainColoring::getColor(const Complex &c) const {
    double ranges = 0;
    double rangee = 1;
    double m = norm(c);
    while (m > rangee) {
        ranges = rangee;
        rangee *= M_E;
    }

    double k = (m - ranges) / (rangee - ranges);

    double argument = arg(c);
    if (argument < 0)
        argument += M_PI * 2;
    argument /= M_PI * 2;

    double sat = (k < 0.5) ? (k * 2) : (1 - (k - 0.5) * 2);
    sat = 1 - pow((1 - sat), 3);
    sat = 0.4 + sat * 0.6;


    double val = (k < 0.5) ? (k * 2) : (1 - (k - 0.5) * 2);
    val = 1 - val;
    val = 1 - pow((1 - val), 3);
    val = 0.6 + val * 0.4;

    return HSBtoRGB((float) argument, (float) sat, (float) val);

}
