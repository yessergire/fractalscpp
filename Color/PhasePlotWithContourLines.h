//
// Created by yessergire on 30.6.2015.
//

#ifndef FRACTALS_PHASE_PLOT_WITH_CONTOUR_LINES_H
#define FRACTALS_PHASE_PLOT_WITH_CONTOUR_LINES_H

#include "Color.h"

class PhasePlotWithContourLines : public Color {
public:
    int getColor(const Complex &c) const override;
};


#endif //FRACTALS_PHASE_PLOT_WITH_CONTOUR_LINES_H
