//
// Created by yessergire on 30.6.2015.
//

#include "ClassicalDomainColoring.h"

int ClassicalDomainColoring::getColor(const Complex &c) const {
    double hue = arg(c) / (2 * M_PI) + 1;
    double brightness = pow(1.0 - 1.0 / (1 + pow(norm(c), 2)), 0.2);
    return HSBtoRGB((float) hue, 1, (float) brightness);
}
