//
// Created by yessergire on 30.6.2015.
//

#ifndef FRACTALS_CLASSICALDOMAINCOLORING_H
#define FRACTALS_CLASSICALDOMAINCOLORING_H

#include "Color.h"

class ClassicalDomainColoring : public Color {

public:
    int getColor(const Complex &c) const override;
};


#endif //FRACTALS_CLASSICALDOMAINCOLORING_H
