//
// Created by yessergire on 30.6.2015.
//

#ifndef FRACTALS_DOMAINCOLORINGWITHCONTOURS_H
#define FRACTALS_DOMAINCOLORINGWITHCONTOURS_H

#include "Color.h"

class DomainColoringWithContours : public Color {

public:
    virtual int getColor(const Complex &c) const override;
};


#endif //FRACTALS_DOMAINCOLORINGWITHCONTOURS_H
