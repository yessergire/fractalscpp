//
// Created by yessergire on 30.6.2015.
//

#include "PhasePlotWithContourLines.h"

double perfract(double x, double t) {
    x = x / t;
    return 0.7 + 0.3 * (x - floor(x));
};

int PhasePlotWithContourLines::getColor(const Complex &c) const {
    double hue = arg(c) / (2 * M_PI) + 1;
    int n = 16; // n=number of isochromatic lines per cycle
    double isol = perfract(hue, 1.0 / n); //# isochromatic lines
    double brightness = log(norm(c));
    double modc = perfract(brightness, 2 * M_PI / n); // lines of constant log-modulus
    return HSBtoRGB((float) hue, 1, (float) (modc * isol));
}
