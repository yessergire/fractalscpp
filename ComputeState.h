//
// Created by yessergire on 27.6.2015.
//

#ifndef FRACTALS_COMPUTESTATE_H
#define FRACTALS_COMPUTESTATE_H

#include "Fractals.h"
#include "Functions/ComplexFunction.h"
#include "Color/Color.h"
#include "Functions/IteratingFunction.h"

class ComputeState {
public:
    ComputeState(const IteratingFunction *complexFunction, const Color *color);

    void setComplexFunction(const IteratingFunction &complexFunction);

    const IteratingFunction * getComplexFunction() const;

    void setColor(const Color &color);

    const Color *getColor() const;

    void setRadius(double radius);

    double getRadius() const;

    void setCenter(const Complex &center);

    Complex getCenter() const;

    int getRGB(int x, int y, int width, int height) const;
    unsigned char getIterations(int x, int y, int width, int height) const;

private:
    float radius = 2;
    Complex center;
    const Color *color;
    const IteratingFunction *complexFunction;
    Complex getPoint(int index, int width, int height) const;
    unsigned char getPointIterations(int index, int width, int height) const;
};


#endif //FRACTALS_COMPUTESTATE_H
