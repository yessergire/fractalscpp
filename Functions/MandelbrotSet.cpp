//
// Created by yessergire on 27.6.2015.
//

#include "MandelbrotSet.h"

Complex MandelbrotSet::calculate(const Complex &c) const {
    Complex z = 0;
    for (int i = 0; norm(z) <= 4 && i < iterations; i++)
        z = (z * z) + c;
    return z;
}

unsigned char MandelbrotSet::calculateIterations(const Complex &c) const {
    Complex z = 0;
    unsigned char i;
    for (i = 0; modulus(z) <= 10 && i < iterations; i++) {
        z = (z * z) + c;
    }
    return i;
}
