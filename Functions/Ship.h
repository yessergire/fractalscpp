//
// Created by yessergire on 1.7.2015.
//

#ifndef FRACTALS_SHIP_H
#define FRACTALS_SHIP_H


#include "IteratingFunction.h"

class Ship : public IteratingFunction {

public:
    Complex calculate(const Complex &c) const override;
    unsigned char calculateIterations(const Complex &c) const override;
};


#endif //FRACTALS_SHIP_H
