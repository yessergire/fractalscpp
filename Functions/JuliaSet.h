//
// Created by yessergire on 30.6.2015.
//

#ifndef FRACTALS_JULIASET_H
#define FRACTALS_JULIASET_H


#include "IteratingFunction.h"

class JuliaSet : public IteratingFunction {

public:
    virtual Complex calculate(const Complex &c) const override;
    virtual unsigned char calculateIterations(const Complex &c) const override;
};


#endif //FRACTALS_JULIASET_H
