//
// Created by yessergire on 30.6.2015.
//

#include "IteratingFunction.h"


IteratingFunction::IteratingFunction(unsigned char iterations) : iterations(iterations) {
}


Complex IteratingFunction::absolute(Complex c) const {
    TYPE im = c.imag();
    TYPE re = c.real();
    if (re < 0) re = -re;
    if (im < 0) im = -im;
    return Complex(re, im);
}

TYPE IteratingFunction::modulus(Complex c) const {
    TYPE im = c.imag();
    TYPE re = c.real();
    return sqrt(re*re + im*im);
}


unsigned char IteratingFunction::getIterations() {
    return iterations;
}

void IteratingFunction::setIterations(unsigned char iterations) {
    this->iterations = iterations;
}
