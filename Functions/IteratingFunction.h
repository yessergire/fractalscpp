//
// Created by yessergire on 30.6.2015.
//

#ifndef FRACTALS_ITERATINGFUNCTION_H
#define FRACTALS_ITERATINGFUNCTION_H


#include "ComplexFunction.h"

class IteratingFunction : public ComplexFunction {
public:
    IteratingFunction(unsigned char iterations = 100);
    virtual unsigned char calculateIterations(const Complex &c) const = 0;
    unsigned char getIterations();
    void setIterations(unsigned char iterations);
    virtual Complex calculate(const Complex &c) const = 0;

protected:
    unsigned char iterations;
    Complex absolute(Complex c) const;
    TYPE modulus(Complex c) const;
};


#endif //FRACTALS_ITERATINGFUNCTION_H
