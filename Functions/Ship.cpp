//
// Created by yessergire on 1.7.2015.
//

#include "Ship.h"



Complex Ship::calculate(const Complex &c) const {
    Complex z = 0;
    for (int i = 0; norm(z) <= 4 && i < iterations; i++) {
        z = absolute(z);
        z = (z * z) + c;
    }
    return z;
}

unsigned char Ship::calculateIterations(const Complex &c) const {
    Complex z = 0;
    unsigned char i;
    for (i = 0; modulus(z) <= 10 && i < iterations; i++) {
        z = absolute(z);
        z = (z * z) + c;
    }
    return i;
}
