//
// Created by yessergire on 30.6.2015.
//

#ifndef FRACTALS_COMPLEXFUNCTION_H
#define FRACTALS_COMPLEXFUNCTION_H

#include "../Fractals.h"

class ComplexFunction {
public:
    virtual Complex calculate(const Complex &c) const = 0;
};


#endif //FRACTALS_COMPLEXFUNCTION_H
