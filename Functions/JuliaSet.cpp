//
// Created by yessergire on 30.6.2015.
//

#include "JuliaSet.h"

Complex JuliaSet::calculate(const Complex &c) const {
    Complex z(c);
    Complex n(-0.59);
    int i;
    for (i = 0; norm(z) <= (1 + sqrt(1 + 4 * norm(z))) / 2 && i < iterations; i++) {
        z = exp(z * z * z) + n;
    }

    return z;
}

unsigned char JuliaSet::calculateIterations(const Complex &c) const {
    Complex z(c);
    Complex n(-0.59);
    unsigned char i;
    for (i = 0; norm(z) <= (1 + sqrt(1 + 4 * norm(z))) / 2 && i < iterations; i++) {
        z = exp(z * z * z) + n;
    }

    return i;
}
