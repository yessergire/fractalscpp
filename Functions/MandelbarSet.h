//
// Created by yessergire on 30.6.2015.
//

#ifndef FRACTALS_MANDELBARSET_H
#define FRACTALS_MANDELBARSET_H


#include "IteratingFunction.h"

class MandelbarSet : public IteratingFunction {

public:
    Complex calculate(const Complex &c) const override;
    unsigned char calculateIterations(const Complex &c) const override;
};

#endif //FRACTALS_MANDELBARSET_H
