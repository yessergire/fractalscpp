//
// Created by yessergire on 30.6.2015.
//

#include "MandelbarSet.h"

Complex MandelbarSet::calculate(const Complex &c) const {
    Complex z = 0;
    for (int i = 0; modulus(z) <= 10 && i < iterations; i++) {
        z = conj(z) * conj(z) + c;
    }
    return z;
}

unsigned char MandelbarSet::calculateIterations(const Complex &c) const {
    Complex z = 0;
    unsigned char i;
    for (i = 0; modulus(z) <= 10 && i < iterations; i++) {
        z = conj(z) * conj(z) + c;
    }
    return i;
}
