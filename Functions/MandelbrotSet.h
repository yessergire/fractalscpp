//
// Created by yessergire on 27.6.2015.
//

#ifndef FRACTALS_MANDELBROTSET_H
#define FRACTALS_MANDELBROTSET_H

#include "../Fractals.h"
#include "IteratingFunction.h"

class MandelbrotSet : public IteratingFunction {

public:
    Complex calculate(const Complex &c) const override;
    unsigned char calculateIterations(const Complex &c) const override;
};


#endif //FRACTALS_MANDELBROTSET_H
